//my code
import React, { Component } from "react";
import MainTodo from './components/MainTodo';
import AboutTodo from './components/AboutTodo';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

const AppNavigator = createMaterialBottomTabNavigator(
  {
    Home: { screen: AboutTodo },
    Todo: { screen: MainTodo },
  },
  {
    barStyle: { backgroundColor: '#ffff' },
    activeColor: 'tomato',
    inactiveColor: 'gray',
    pressColor: 'pink',
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = "home";
        } else if (routeName === 'Todo') {
          iconName = "check-circle";
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Icon name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
); // CreateBottomTabNavigator
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;
