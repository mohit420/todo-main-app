import { StyleSheet, Dimensions } from 'react-native';
const isAndroid = Platform.OS == "android";
import { Platform } from "react-native";
export default styles = StyleSheet.create({

    ////////MainTodo.js////////////////////
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffff",
        width: "100%"
    },
    flatlist: {
        flex: 1,
        width: "95%",
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#fff',
        paddingTop: 5,
        paddingBottom: 5,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: "#b3cccc",
        marginBottom: 10,
    },
    list: {
        width: "100%"
    },
    listItem: {
        fontSize: 15,
        fontFamily: 'serif',
        marginTop: 5,
        marginBottom: 5,
        textAlign: "left",
        paddingTop: 2,
        paddingBottom: 2,
    },
    listItem_Container: {
        flex: 1,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 15,
        paddingLeft: 10,
        marginTop: 5,
        marginBottom: 5,
        marginRight: 20,
        marginLeft: 5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between"
    },
    hr: {
        height: 2,
        backgroundColor: "white"
    },
    textInput: {
        fontSize: 15,
        fontFamily: 'Roboto',
        alignItems: "center",
        textAlign: "center",
    },
    textinput_container: {
        //padding: 10,
        //marginTop: 10,
        width: "100%",
        backgroundColor: '#3399ff',
        paddingRight: "10%",
        paddingLeft: "10%",
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#fff',
    },

    //////////////////AboutTodo.js///////////////////////////////////////

    AboutTodo_Container: {
        flex: 1,
        //marginTop: 20,
        paddingBottom: 5,
        backgroundColor: '#6a9eec',
    },
    MainHeader: {
        fontWeight: 'bold',
        fontFamily: 'sans-serif',
        textAlign: 'center',
        color: '#ff4d4d',
        flex: 1,
        flexDirection: 'row',
    },
    headertext: {
        fontWeight: 'bold',
        fontFamily: 'serif',
        width: "100%",
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#fff',
        paddingTop: 5,
        marginBottom: 10,
        color: '#ff4d4d',
        fontSize: 25,
        textAlign: 'center'
    },
    main_Headingtext: {
        flex: 1,
        marginTop: 30,
        fontWeight: 'bold',
        color: "#fff",
        fontFamily: 'sans-serif-medium',
        textAlign: 'center',
        fontSize: 19,
        paddingLeft: 10,
        paddingRight: 10,
    },
    hearttext: {
        fontWeight: 'bold',
        fontFamily: 'notoserif',
        textAlign: 'center',
        alignItems: "center",
        color: "#fff",
        fontSize: 15,
        marginBottom: 10,
        marginTop: 10,
        flex: 1,
        flexDirection: 'row',
    },

    /////////////////// TwitterButton.js ////////////////////

    twitterbutton: {
        marginRight: 70,
        marginLeft: 70,
        marginTop: 5,
        marginBottom: 10,
        flexDirection: 'row',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#FFDAB9',
        backgroundColor: '#4da6ff',
        alignItems: 'center',
        paddingBottom: 2,
    },
    twittertext: {
        alignItems: 'center',
        flex: 1,
        fontWeight: 'bold',
        color: '#ffffff',
        fontFamily: 'serif',
        textAlign: 'center',
        fontSize: 15,
    },

});