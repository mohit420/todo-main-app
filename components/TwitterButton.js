import React, {
    Component
} from 'react';
import {
    AppRegistry,
    ScrollView,
    StyleSheet,
    Header,
    TextInput,
    ActivityIndicator,
    Linking,
    TouchableOpacity,
    View,
    Modal,
    Alert,
    Button,
    scrollView,
    Image,
    Text,
} from 'react-native';
// my code import
import styles from '../themes/styles';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class HomeView extends Component {
    render() {
        return (
            <View style={styles.twitterbutton}>
                <Text style={styles.twittertext} onPress={() => Linking.openURL('https://twitter.com/Mohit85478329')} ><Icon name="twitter" size={18} color='#ffff' /> Follow @Mohit</Text>
            </View>

        );
    }
}
