import React, {
    Component
} from 'react';
import {
    AppRegistry,
    ScrollView,
    StyleSheet,
    Header,
    TextInput,
    ActivityIndicator,
    TouchableOpacity,
    View,
    Modal,
    Alert,
    Button,
    scrollView,
    Image,
    Text,
} from 'react-native';
// my code imports
import Icon from 'react-native-vector-icons/FontAwesome';
import TwitterButton from './TwitterButton';
import styles from '../themes/styles';

export default class AboutTodo extends Component {
    render() {
        return (
            <View style={styles.AboutTodo_Container}>
                <ScrollView>
                    <View>
                        <View style={styles.MainHeader}>
                            <Text style={styles.headertext}>TODO</Text>
                        </View>
                        <View>
                            <Text style={styles.main_Headingtext}>Todo App is a simple app for making Todo lists, rather than in writing with the pen on the paper, you are using the digital User Interface to write your own tasks in a faster, easy to handle, time-consuming and with no loss tension for your day-to-day tasks.</Text>
                        </View>
                        <View
                            style={{
                                borderBottomColor: '#8d9ba5',
                                borderBottomWidth: 2,
                                marginLeft: 20,
                                marginRight: 20,
                                marginTop: 40,
                            }}
                        />
                        <View>
                            <Text style={styles.hearttext}>
                                Made with <Icon name="heart" size={18} color='red' />
                            </Text>
                        </View>
                        <View>
                            <TwitterButton />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
