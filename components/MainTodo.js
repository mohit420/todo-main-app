import React, { Component } from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    FlatList,
    AsyncStorage,
    ScrollView,
    Button,
    TextInput,
    Keyboard,
    Platform
} from "react-native";
// my code
import styles from '../themes/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
const BLUE = "#ff0066";
const LIGHT_GRAY = "#fff";
export default class MainTodo extends Component {
    state = {
        tasks: [],
        text: "",
        isFocused: false,
    };

    handleFocus = event => {
        this.setState({ isFocused: true });
        if (this.props.onFocus) {
            this.props.onFocus(event);
        }
    };

    handleBlur = event => {
        this.setState({ isFocused: false });
        if (this.props.onBlur) {
            this.props.onBlur(event);
        }
    };

    changeTextHandler = text => {
        this.setState({ text: text });
    };

    addTask = () => {
        let notEmpty = this.state.text.trim().length > 0;
        if (notEmpty) {
            this.setState(
                prevState => {
                    let { tasks, text } = prevState;
                    return {
                        tasks: tasks.concat({ key: tasks.length, text: text }),
                        text: ""
                    };
                },
                () => Tasks.save(this.state.tasks)
            );
        }
    };

    deleteTask = i => {
        this.setState(
            prevState => {
                let tasks = prevState.tasks.slice();

                tasks.splice(i, 1);

                return { tasks: tasks };
            },
            () => Tasks.save(this.state.tasks)
        );
    };

    componentDidMount() {
        Tasks.all(tasks => this.setState({ tasks: tasks || [] }));
    }

    render() {
        const { isFocused } = this.state;
        return (
            <View style={styles.container} >
                <View style={styles.textinput_container}>
                    <TextInput
                        style={styles.textInput}
                        underlineColorAndroid={
                            isFocused ? BLUE : LIGHT_GRAY
                        }
                        placeholder="Email"
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        onChangeText={this.changeTextHandler}
                        onSubmitEditing={this.addTask}
                        value={this.state.text}
                        placeholder="Add Tasks"
                        returnKeyType="done"
                        returnKeyLabel="done"
                    />
                </View>
                <FlatList
                    style={styles.flatlist}
                    keyExtractor={(item) => item.key.toString()}
                    data={this.state.tasks}
                    renderItem={({ item, index }) =>
                        <View>
                            <View style={styles.listItem_Container}>
                                <Text style={styles.listItem}>
                                    {item.text}
                                </Text>
                                <Icon name="trash" size={25} onPress={() => this.deleteTask(index)} />
                            </View>
                            <View style={styles.hr} />
                        </View>}
                />
            </View>
        );
    }
}

let Tasks = {
    convertToArrayOfObject(tasks, callback) {
        return callback(
            tasks ? tasks.split("||").map((task, i) => ({ key: i, text: task })) : []
        );
    },
    convertToStringWithSeparators(tasks) {
        return tasks.map(task => task.text).join("||");
    },
    all(callback) {
        return AsyncStorage.getItem("TASKS", (err, tasks) =>
            this.convertToArrayOfObject(tasks, callback)
        );
    },
    save(tasks) {
        AsyncStorage.setItem("TASKS", this.convertToStringWithSeparators(tasks));
    }
};